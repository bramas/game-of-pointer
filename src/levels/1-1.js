

export default {
    name: '1-1',
    initialMemory:{ 
    'main': {
        name:'main',
        variables:{
        'a': {type:'int', index:0}
        },
        nextIndex: 4,
        minValuesCount: 8,
        values: [
        1,
        2,
        52,
        3
        ]
    }
    },
    expScore: 2,
    memScore: 4,
    checks: [
    { 
        color: 'blue',
        memoryName: 'main',
        index: 0,
        value: 42
    }
    ],
    readonly: `
int main(void)
{
    int a;`
}