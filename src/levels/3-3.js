

export default {
    name: '3-3',
    initialMemory:{ 
    'main': {
        name:'main',
        variables:{},
        readonly:[0,2],
        nextIndex: 0,
        minValuesCount: 4,
        values: []
    },
    'foo': {
        name:'foo',
        variables:{},
        readonly:[],
        nextIndex: 0,
        minValuesCount: 4,
        values: []
    }
    },
    expScore: 5,
    memScore: 7,
    checks: [
    { 
        color: 'green-o',
        memoryName: 'main',
        index: 2,
        links:[
            { 
                color: 'green-i',
                memoryName: 'main',
                index: 0,
            },{ 
                color: 'green-i',
                memoryName: 'main',
                index: 1,
            }
        ]
    }
    ],
    readonly: `
int main(void)
{
    unsigned char a =1;
    unsigned char b = randInt() >> 15;
    unsigned char c = randInt() >> 15;
    foo(&a);
}`
}
