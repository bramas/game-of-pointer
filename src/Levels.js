

import Levels from './levels/*.js';

Object.keys(Levels).forEach(k => Levels[k] = Levels[k].default);

export default Levels;
